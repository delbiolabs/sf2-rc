# docs: http://johnkary.net/blog/useful-commands-for-efficient-symfony2-development/

# symfony console command
alias sf='php app/console'

# symfony console command in debug
alias sfd='php-cli-debug app/console'

# comando per resettare la cache
alias sfc='sf ca:cl'

alias dcc='sf doctrine:cache:clear-metadata && sf doctrine:cache:clear-query && sf doctrine:cache:clear-result'

# cancella tutte le cache di symfony e doctrine
alias sfca='dcc && sfc --env=dev && sfc --env=prod && sfc --env=test'

# symfony test
alias sft='php bin/phpunit -c app/'

# symfony test in debug
alias sfdt='php-cli-debug bin/phpunit -c app/'
