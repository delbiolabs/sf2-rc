#!/usr/bin/env bash
# docs: http://johnkary.net/blog/useful-commands-for-efficient-symfony2-development/

# symfony console command
function sf ()
{
  php app/console "$@"
}
# symfony console command in debug
function sfd ()
{
  php-cli-debug app/console "$@"
}
# comando per resettare la cache
function sfc ()
{
  sf ca:cl "$@"
}
function dcc ()
{
  sf doctrine:cache:clear-metadata \
  && sf doctrine:cache:clear-query \
  && sf doctrine:cache:clear-result
}
# cancella tutte le cache di symfony e doctrine
function sfca ()
{
  dcc && sfc --env=dev \
  && sfc --env=prod \
  && sfc --env=test
}
# symfony test
function sft ()
{
  php bin/phpunit -c app/ "$@"
}
# symfony test in debug
function sfdt ()
{
  php-cli-debug bin/phpunit -c app/ "$@"
}
