#!/usr/bin/env bash

set -x

# doc http://www.sessiondigital.de/blog/connecting-xdebug-to-phpstorm/

php_fpm_ini_file=/etc/php5/fpm/php.ini
php_cli_ini_file=/etc/php5/cli/php.ini
php_cli_debug_launcher_file=/usr/local/bin/php-cli-debug



function change_settings()
{
  if [ ! -z "$XDEBUG_HOST_MACHINE_IP" ]; then
      sed -i "s/^\(xdebug.remote_host=\).*/\1$XDEBUG_HOST_MACHINE_IP/" ${php_fpm_ini_file}
      sed -i "s/^\(xdebug.remote_host=\).*/\1$XDEBUG_HOST_MACHINE_IP/" ${php_cli_ini_file}
      sed -i "s/^\(cli_xdebug_host=\).*/\1$XDEBUG_HOST_MACHINE_IP/" ${php_cli_debug_launcher_file}
  fi

  if [ ! -z "$XDEBUG_REMOTE_PORT" ]; then
      sed -i "s/^\(xdebug.remote_port=\).*/\1$XDEBUG_REMOTE_PORT/" ${php_fpm_ini_file}
      sed -i "s/^\(xdebug.remote_port=\).*/\1$XDEBUG_REMOTE_PORT/" ${php_cli_ini_file}
  fi

  if [ ! -z "$XDEBUG_IDE_KEY" ]; then
      sed -i "s/^\(xdebug.idekey=\).*/\1$XDEBUG_IDE_KEY/" ${php_fpm_ini_file}
      sed -i "s/^\(xdebug.idekey=\).*/\1$XDEBUG_IDE_KEY/" ${php_cli_ini_file}
      sed -i "s/^\(cli_xdebug_idekey=\).*/\1$XDEBUG_IDE_KEY/" ${php_cli_debug_launcher_file}
  fi

  if [ ! -z "$PHP_IDE_CONFIG_SERVER_NAME" ]; then
      sed -i "s/^\(php_ide_config_server_name=\).*/\1$PHP_IDE_CONFIG_SERVER_NAME/" ${php_cli_debug_launcher_file}
  fi
}

function install()
{
  cat cli.ini >> ${php_cli_ini_file}
  cat fpm.ini >> ${php_fpm_ini_file}
}

install
